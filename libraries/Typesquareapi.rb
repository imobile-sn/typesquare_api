require 'net/http'
require 'uri'
require 'json'

module Typesquareapi
  def get_list(api_key, project_id)
    uri = URI('https://api.typesquare.com/v2/projects/' + project_id + '/sites')
    req = Net::HTTP::Get.new(uri)
    req['Authorization'] = 'Bearer ' + api_key
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true) {|http|
      http.request(req)
    }
    eval(res.body)[:data][:sites]
  end
  module_function :get_list

  def add_url(api_key, project_id, fqdn)
    uri = URI('https://api.typesquare.com/v2/projects/' + project_id + '/sites')
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = 'Bearer ' + api_key
    payload = {'url' => ['http://' + fqdn]}.to_json
    req.body = payload
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true) {|http|
      http.request(req)
    }
    res.code == '201' ? true : raise('ERROR: ' + res.body)
  end

  def del_url(api_key, project_id, fqdn)
    target = false
    get_list(api_key, project_id).each {|obj|
      target = obj if obj[:url] == fqdn
    }
    if target
      uri = URI('https://api.typesquare.com/v2/projects/' + project_id + '/sites/' + target[:id].to_s)
      req = Net::HTTP::Delete.new(uri)
      req['Authorization'] = 'Bearer ' + api_key
      res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true) {|http|
        http.request(req)
      }
    else
      raise('not found ' + fqdn)
    end
  end
end
