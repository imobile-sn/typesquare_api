name 'typesquare_api'
maintainer 'The Authors'
maintainer_email 'kazuma.iwasawa@imobile.co.jp'
license 'all_rights'
description 'Installs/Configures typesquare_api'
long_description 'Installs/Configures typesquare_api'
version '1.0.2'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/typesquare_api/issues' if respond_to?(:issues_url)

# The `source_url` points to the development reposiory for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/typesquare_api' if respond_to?(:source_url)
