#
# Cookbook:: typesquare_api
# Recipe:: list
#
# Copyright:: 2017, The Authors, All Rights Reserved.

::Chef::Recipe.send(:include, Typesquareapi)

response = "#{get_list(node['typesquare']['api_key'], node['typesquare']['project_id'])}"

Chef::Log.logger.info response
