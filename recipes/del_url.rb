#
# Cookbook:: typesquare_api
# Recipe:: add_url
#
# Copyright:: 2017, The Authors, All Rights Reserved.

::Chef::Recipe.send(:include, Typesquareapi)

response = "#{del_url(node['typesquare']['api_key'], node['typesquare']['project_id'], node['typesquare']['fqdn'])}"

Chef::Log.logger.info response
